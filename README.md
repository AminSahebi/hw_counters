AArch64 PMU register summary
The PMU counters and their associated control registers are accessible in the AArch64 Execution state with MRS and MSR instructions.
Table below gives a summary of the Cortex-A53 PMU registers in the AArch64 Execution state.

Reference: https://developer.arm.com/documentation/ddi0500/d/performance-monitor-unit/aarch64-pmu-register-summary

## PMU register summary in the AArch64 Execution state
| Name            | Type | Width | Description                                                   |
| :-------------- | :--- | :---- | :------------------------------------------------------------ |
| PMCR\_EL0       | RW   | 32    | *Performance Monitors Control Register*                       |
| PMCNTENSET\_EL0 | RW   | 32    | Performance Monitors Count Enable Set Register                |
| PMCNTENCLR\_EL0 | RW   | 32    | Performance Monitors Count Enable Clear Register              |
| PMOVSCLR\_EL0   | RW   | 32    | Performance Monitors Overflow Flag Status Register            |
| PMSWINC\_EL0    | WO   | 32    | Performance Monitors Software Increment Register              |
| PMSELR\_EL0     | RW   | 32    | Performance Monitors Event Counter Selection Register         |
| PMCEID0\_EL0    | RO   | 32    | *Performance Monitors Common Event Identification Register 0* |
| PMCEID1\_EL0    | RO   | 32    | *Performance Monitors Common Event Identification Register 1* |
| PMCCNTR\_EL0    | RW   | 64    | Performance Monitors Cycle Count Register                     |
| PMXEVTYPER\_EL0 | RW   | 32    | Performance Monitors Selected Event Type and Filter Register  |
| PMCCFILTR\_EL0  | RW   | 32    | Performance Monitors Cycle Count Filter Register              |
| PMXEVCNTR0\_EL0 | RW   | 32    | Performance Monitors Selected Event Count Register            |
| PMUSERENR\_EL0  | RW   | 32    | Performance Monitors User Enable Register                     |
| PMINTENSET\_EL1 | RW   | 32    | Performance Monitors Interrupt Enable Set Register            |
| PMINTENCLR\_EL1 | RW   | 32    | Performance Monitors Interrupt Enable Clear Register          |
| PMOVSSET\_EL0   | RW   | 32    | Performance Monitors Overflow Flag Status Set Register        |
| PMEVCNTR0\_EL0  | RW   | 32    | Performance Monitors Event Count Registers                    |
| PMEVCNTR1\_EL0  | RW   | 32    |                                                               |
| PMEVCNTR2\_EL0  | RW   | 32    |                                                               |
| PMEVCNTR3\_EL0  | RW   | 32    |                                                               |
| PMEVCNTR4\_EL0  | RW   | 32    |                                                               |
| PMEVCNTR5\_EL0  | RW   | 32    |                                                               |
| PMEVTYPER0\_EL0 | RW   | 32    | Performance Monitors Event Type Registers                     |
| PMEVTYPER1\_EL0 | RW   | 32    |                                                               |
| PMXVTYPER2\_EL0 | RW   | 32    |                                                               |
| PMEVTYPER3\_EL0 | RW   | 32    |                                                               |
| PMEVTYPER4\_EL0 | RW   | 32    |                                                               |
| PMEVTYPER5\_EL0 | RW   | 32    |                                                               |
| PMCCFILTR\_EL0  | RW   | 32    | Performance Monitors Cycle Count Filter Register              |

## using Perf
Installation using :

    git clone --depth 1 https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
    cd linux/tools/perf
    make
    cp perf /usr/bin
then to give premission to have access to hardware counters:

    sudo sh -c 'echo -1 >/proc/sys/kernel/perf_event_paranoid'
    sudo sh -c 'echo 0 > /proc/sys/kernel/nmi_watchdog'
	
    sudo apt-get install linux-tools-4.15.0-106-generic linux-cloud-tools-4.15.0-106-generic linux-tools-generic linux-cloud-tools-generic
	